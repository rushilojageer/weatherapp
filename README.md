# README #

Submission for DVT sample Android application by Rushil O'jageer.
Minimum API Version: API 23 Target API Version: API 25

###APK###

You can download the APK https://bitbucket.org/rushilojageer/weatherapp/raw/76b85b348771a11190f6a0a3a94bd27a22abafd2/RushilOjageerWeatherApp.apk

###Issues###

Please email rushilojageer@gmail.com if you have issues running this application.