package com.weather.rushilojageer.weatherapplication.data.remote;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.weather.rushilojageer.weatherapplication.config.Config;
import com.weather.rushilojageer.weatherapplication.data.WeatherDataSource;
import com.weather.rushilojageer.weatherapplication.data.remote.dto.WeatherResponse;
import com.weather.rushilojageer.weatherapplication.domain.WeatherInfo;

import org.json.JSONObject;

public class RemoteWeatherDataSource implements WeatherDataSource {

    private static final String TAG = RemoteWeatherDataSource.class.getName();
    private Context context;

    public RemoteWeatherDataSource(Context context) {

        this.context = context;
    }

    @Override
    public void getWeatherInfo(double latitude, double longitude, @NonNull final WeatherInfoCallback callback) {
        Log.i(TAG,"Requesting weather information...");
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = String.format("%s/weather?lat=%s&lon=%s&appid=%s", Config.BASE_WEATHER_URL, latitude, longitude, Config.WEATHER_API_KEY);

        JsonObjectRequest request = new JsonObjectRequest(url, null,
            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Gson gson = new GsonBuilder().create();
                        WeatherResponse weatherResponse = gson.fromJson(response.toString(), WeatherResponse.class);
                        WeatherInfo weatherInfo = new WeatherInfo();
                        weatherInfo.setMaxTemperature(weatherResponse.getMain().getMaximumTemperature());
                        weatherInfo.setMinTemperature(weatherResponse.getMain().getMinimumTemperature());
                        callback.hideProgress();
                        callback.onWeatherInfoLoaded(weatherInfo);
                    } catch (JsonSyntaxException ex) {
                        Log.e(TAG, String.format("Unable to parse weather JSON data - %s", ex.getMessage()));
                        callback.hideProgress();
                        callback.onError();
                    }
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, String.format("Unable to retrieve weather JSON data - %s", error.getMessage()));
                    callback.hideProgress();
                    callback.onError();
                }
            }
        );

        queue.add(request);
        callback.showProgress();
    }

}
