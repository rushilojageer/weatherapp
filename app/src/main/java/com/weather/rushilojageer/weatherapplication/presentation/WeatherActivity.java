package com.weather.rushilojageer.weatherapplication.presentation;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.weather.rushilojageer.weatherapplication.R;
import com.weather.rushilojageer.weatherapplication.data.remote.RemoteLocationDataSource;
import com.weather.rushilojageer.weatherapplication.data.remote.RemoteWeatherDataSource;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WeatherActivity extends Activity implements
        WeatherInfoContract.View,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private static final int PERMISSION_ACCESS_FINE_LOCATION = 1;
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 900000;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 300000;

    @BindView(R.id.content_view)
    protected ScrollView contentView;

    @BindView(R.id.loading_view)
    protected LinearLayout loadingView;

    @BindView(R.id.error_view)
    protected LinearLayout errorView;

    @BindView(R.id.date)
    protected TextView date;

    @BindView(R.id.min_temperature)
    protected TextView minTemperature;

    @BindView(R.id.max_temperature)
    protected TextView maxTemperature;

    @BindView(R.id.location)
    protected TextView location;

    @BindView(R.id.error)
    protected TextView error;

    @BindView(R.id.enable_location_permission)
    protected Button enableLocationPermission;

    private GoogleApiClient mGoogleApiClient;
    private WeatherInfoContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        isGooglePlayServicesAvailable(this);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        presenter = new WeatherPresenter(new RemoteWeatherDataSource(this), new RemoteLocationDataSource(this));
        presenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ACCESS_FINE_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setupLocationUpdates();
                } else {
                    showErrorView(getString(R.string.enable_location_message), true);
                }
                break;
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[] { android.Manifest.permission.ACCESS_FINE_LOCATION },
                    PERMISSION_ACCESS_FINE_LOCATION);
        }

        setupLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        showErrorView(getString(R.string.generic_error), false);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        showErrorView(getString(R.string.generic_error), false);
    }


    @Override
    public void onLocationChanged(Location location) {
        presenter.loadWeatherInfo(location.getLatitude(), location.getLongitude());
        presenter.loadLocationInfo(location.getLatitude(), location.getLongitude());
    }

    @OnClick(R.id.enable_location_permission)
    public void onEnableLocationPermissionClick() {
        ActivityCompat.requestPermissions(this, new String[] { android.Manifest.permission.ACCESS_FINE_LOCATION },
                PERMISSION_ACCESS_FINE_LOCATION);
    }

    @Override
    public void showLoadingView() {
        this.loadingView.setVisibility(View.VISIBLE);
        this.contentView.setVisibility(View.GONE);
        this.errorView.setVisibility(View.GONE);
    }

    @Override
    public void hideLoadingView() {
        this.loadingView.setVisibility(View.GONE);
    }

    @Override
    public void showWeatherInfo(String date, String minTemp, String maxTemp) {
        this.contentView.setVisibility(View.VISIBLE);
        this.loadingView.setVisibility(View.GONE);
        this.errorView.setVisibility(View.GONE);
        this.date.setText(formatDate(R.string.today, date));
        this.minTemperature.setText(formatTemperature(R.string.min, minTemp));
        this.maxTemperature.setText(formatTemperature(R.string.max, maxTemp));
    }

    @Override
    public void showLocationInfo(String location) {
        if (location == null) {
            this.location.setText(R.string.unable_to_determine_address);
            return;
        }

        this.location.setText(location);
    }

    @Override
    public void showErrorView(String errorMessage, boolean showEnableLocationPermissionButton) {
        this.errorView.setVisibility(View.VISIBLE);
        this.loadingView.setVisibility(View.GONE);
        this.contentView.setVisibility(View.GONE);
        this.enableLocationPermission.setVisibility(showEnableLocationPermissionButton ? View.VISIBLE : View.GONE);
        this.error.setText(errorMessage);
    }

    @Override
    public void showWeatherError() {
        showErrorView(getString(R.string.unable_to_obtain_weather_info), false);
    }

    @Override
    public void showLoadingLocation() {
        this.location.setText(R.string.finding_your_address);
    }

    @Override
    public void showLocationError() {
        this.location.setText(R.string.unable_to_determine_address);
    }

    private void setupLocationUpdates() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if(location == null) {
            LocationRequest singleLocationRequest = LocationRequest.create()
                    .setNumUpdates(1)
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                    .setInterval(0);

            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, singleLocationRequest, this, null);
        } else {
            presenter.loadWeatherInfo(location.getLatitude(), location.getLongitude());
            presenter.loadLocationInfo(location.getLatitude(), location.getLongitude());
        }

        LocationRequest locationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(UPDATE_INTERVAL_IN_MILLISECONDS)
            .setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
    }

    private String formatTemperature(int labelId, String temperature) {
        String label = getString(labelId);
        return String.format("%s %s\u00B0C", label, temperature);
    }

    private String formatDate(int labelId, String date) {
        String label = getString(labelId);
        return String.format("%s, %s", label, date);
    }

    private boolean isGooglePlayServicesAvailable(Activity activity) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
        if(status != ConnectionResult.SUCCESS) {
            if(googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(activity, status, 2404).show();
            }
            return false;
        }
        return true;
    }
}
