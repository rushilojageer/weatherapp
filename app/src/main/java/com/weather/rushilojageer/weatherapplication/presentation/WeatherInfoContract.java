package com.weather.rushilojageer.weatherapplication.presentation;

interface WeatherInfoContract {

    interface View {
        void showLoadingView();
        void hideLoadingView();
        void showWeatherInfo(String date, String minTemp, String maxTemp);
        void showLocationInfo(String location);
        void showErrorView(String errorMessage, boolean showEnableLocationPermissionButton);
        void showWeatherError();
        void showLoadingLocation();
        void showLocationError();
    }

    interface Presenter {
        void attachView(View view);
        void detachView();
        void loadWeatherInfo(double latitude, double longitude);
        void loadLocationInfo(double latitude, double longitude);
    }

}
