package com.weather.rushilojageer.weatherapplication.data;

import android.support.annotation.NonNull;

import com.weather.rushilojageer.weatherapplication.domain.WeatherInfo;

public interface WeatherDataSource {

    interface WeatherInfoCallback {
        void showProgress();
        void hideProgress();
        void onWeatherInfoLoaded(WeatherInfo weatherInfo);
        void onError();
    }

    void getWeatherInfo(double latitude, double longitude, @NonNull WeatherInfoCallback callback);

}
