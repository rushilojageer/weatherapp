package com.weather.rushilojageer.weatherapplication.data.remote;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.weather.rushilojageer.weatherapplication.config.Config;
import com.weather.rushilojageer.weatherapplication.data.LocationDataSource;
import com.weather.rushilojageer.weatherapplication.data.remote.dto.LocationResponse;
import com.weather.rushilojageer.weatherapplication.domain.LocationInfo;

import org.json.JSONObject;

import static com.weather.rushilojageer.weatherapplication.data.remote.dto.LocationResponse.AddressComponent.ADMINISTRATIVE_AREA_LEVEL_1;
import static com.weather.rushilojageer.weatherapplication.data.remote.dto.LocationResponse.AddressComponent.ADMINISTRATIVE_AREA_LEVEL_2;
import static com.weather.rushilojageer.weatherapplication.data.remote.dto.LocationResponse.AddressComponent.COUNTRY;
import static com.weather.rushilojageer.weatherapplication.data.remote.dto.LocationResponse.AddressComponent.LOCALITY;
import static com.weather.rushilojageer.weatherapplication.data.remote.dto.LocationResponse.AddressComponent.OK;
import static com.weather.rushilojageer.weatherapplication.data.remote.dto.LocationResponse.AddressComponent.SUB_LOCALITY_LEVEL_1;
import static com.weather.rushilojageer.weatherapplication.data.remote.dto.LocationResponse.AddressComponent.ZERO_RESULTS;

public class RemoteLocationDataSource implements LocationDataSource {

    private static final String TAG = RemoteLocationDataSource.class.getName();
    private Context context;

    public RemoteLocationDataSource(Context context) {

        this.context = context;
    }

    @Override
    public void getLocationInfo(double latitude, double longitude, @NonNull final LocationInfoCallback callback) {
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = String.format("%s?latlng=%s,%s&key=%s", Config.BASE_GEOCODE_URL, latitude, longitude, Config.GEOCODE_API_KEY);

        JsonObjectRequest request = new JsonObjectRequest(url, null,
            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Gson gson = new GsonBuilder().create();
                        LocationResponse locationResponse = gson.fromJson(response.toString(), LocationResponse.class);
                        String status = locationResponse.getStatus().toLowerCase();
                        LocationInfo locationInfo = new LocationInfo();

                        if (ZERO_RESULTS.equals(status)) {
                            callback.hideProgress();
                            callback.onLocationInfoLoaded(locationInfo);
                            return;
                        }

                        if (!OK.equals(status)) {
                            callback.hideProgress();
                            callback.onError();
                            return;
                        }

                        if (locationResponse.getResults().length <= 0) {
                            callback.hideProgress();
                            callback.onError();
                            return;
                        }

                        LocationResponse.AddressComponent[] addressComponents = locationResponse.getResults()[0].getAddressComponents();
                        for (LocationResponse.AddressComponent addressComponent : addressComponents) {
                            for (String addressComponentType : addressComponent.getAddressComponentTypes()) {
                                String type = addressComponentType.toLowerCase();

                                if (SUB_LOCALITY_LEVEL_1.equals(type)) {
                                    locationInfo.setSubLocality(addressComponent.getShortName());
                                    break;
                                }
                                if (LOCALITY.equals(type)) {
                                    locationInfo.setLocality(addressComponent.getShortName());
                                    break;
                                }
                                if (ADMINISTRATIVE_AREA_LEVEL_1.equals(type)) {
                                    locationInfo.setAdministrativeAreaLevelOne(addressComponent.getShortName());
                                    break;
                                }
                                if (ADMINISTRATIVE_AREA_LEVEL_2.equals(type)) {
                                    locationInfo.setAdministrativeAreaLevelTwo(addressComponent.getShortName());
                                    break;
                                }
                                if (COUNTRY.equals(type)) {
                                    locationInfo.setCountry(addressComponent.getLongName());
                                    break;
                                }
                            }
                        }

                        callback.hideProgress();
                        callback.onLocationInfoLoaded(locationInfo);
                    } catch (JsonSyntaxException ex) {
                        Log.e(TAG, String.format("Unable to parse location JSON data - %s", ex.getMessage()));
                        callback.hideProgress();
                        callback.onError();
                    }
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, String.format("Unable to retrieve weather JSON data - %s", error.getMessage()));
                    callback.hideProgress();
                    callback.onError();
                }
            }
        );

        queue.add(request);
        callback.showProgress();
    }
}
