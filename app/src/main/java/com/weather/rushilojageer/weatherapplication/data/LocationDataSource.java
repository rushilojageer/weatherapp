package com.weather.rushilojageer.weatherapplication.data;

import android.support.annotation.NonNull;

import com.weather.rushilojageer.weatherapplication.domain.LocationInfo;

public interface LocationDataSource {

    interface LocationInfoCallback {
        void showProgress();
        void hideProgress();
        void onLocationInfoLoaded(LocationInfo locationInfo);
        void onError();
    }

    void getLocationInfo(double latitude, double longitude, @NonNull LocationInfoCallback callback);

}
