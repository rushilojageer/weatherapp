package com.weather.rushilojageer.weatherapplication.data.remote.dto;

import com.google.gson.annotations.SerializedName;

public class LocationResponse {

    @SerializedName("status")
    private String status;

    @SerializedName("results")
    private Result [] results;

    public String getStatus() {
        return status;
    }

    public Result[] getResults() {
        return results;
    }

    public class Result {

        @SerializedName("address_components")
        private AddressComponent [] addressComponents;

        public AddressComponent[] getAddressComponents() {
            return addressComponents;
        }
    }

    public class AddressComponent {

        public static final String ZERO_RESULTS = "zero_results";
        public static final String OK = "ok";
        public static final String SUB_LOCALITY_LEVEL_1 = "sublocality_level_1";
        public static final String LOCALITY = "locality";
        public static final String ADMINISTRATIVE_AREA_LEVEL_1 = "administrative_area_level_1";
        public static final String ADMINISTRATIVE_AREA_LEVEL_2 = "administrative_area_level_2";
        public static final String COUNTRY = "country";

        @SerializedName("short_name")
        private String shortName;

        @SerializedName("long_name")
        private String longName;

        @SerializedName("types")
        private String [] addressComponentTypes;

        public String getShortName() {
            return shortName;
        }

        public String getLongName() {
            return longName;
        }

        public String[] getAddressComponentTypes() {
            return addressComponentTypes;
        }
    }
}
