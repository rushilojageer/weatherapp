package com.weather.rushilojageer.weatherapplication.config;

public class Config {

    public static final String BASE_WEATHER_URL = "http://api.openweathermap.org/data/2.5";
    public static final String WEATHER_API_KEY = "8449616fb341f902d8ca7eabf7f31383";
    public static final String BASE_GEOCODE_URL = "https://maps.googleapis.com/maps/api/geocode/json";
    public static final String GEOCODE_API_KEY = "AIzaSyBb8YQH37mIlXMe2j5yhMBLC-pof65ZnDs";
}
