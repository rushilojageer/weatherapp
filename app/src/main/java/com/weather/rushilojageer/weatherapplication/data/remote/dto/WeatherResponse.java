package com.weather.rushilojageer.weatherapplication.data.remote.dto;

import com.google.gson.annotations.SerializedName;

public class WeatherResponse {

    @SerializedName("main")
    private Main main;

    public Main getMain() {
        return main;
    }

    public class Main {

        @SerializedName("temp_min")
        private double minimumTemperature;

        @SerializedName("temp_max")
        private double maximumTemperature;

        public double getMinimumTemperature() {
            return minimumTemperature;
        }

        public double getMaximumTemperature() {
            return maximumTemperature;
        }
    }
}
