package com.weather.rushilojageer.weatherapplication.domain;

public class LocationInfo {

    private String subLocality;
    private String locality;
    private String administrativeAreaLevelOne;
    private String administrativeAreaLevelTwo;
    private String country;

    public String getSubLocality() {
        return subLocality;
    }

    public void setSubLocality(String subLocality) {
        this.subLocality = subLocality;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getAdministrativeAreaLevelOne() {
        return administrativeAreaLevelOne;
    }

    public void setAdministrativeAreaLevelOne(String administrativeAreaLevelOne) {
        this.administrativeAreaLevelOne = administrativeAreaLevelOne;
    }

    public String getAdministrativeAreaLevelTwo() {
        return administrativeAreaLevelTwo;
    }

    public void setAdministrativeAreaLevelTwo(String administrativeAreaLevelTwo) {
        this.administrativeAreaLevelTwo = administrativeAreaLevelTwo;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getArea() {
        if (subLocality != null)
            return subLocality;
        else if (locality != null)
            return locality;
        else if (administrativeAreaLevelTwo != null)
            return administrativeAreaLevelTwo;
        else if (administrativeAreaLevelOne != null)
            return administrativeAreaLevelOne;
        else
            return null;
    }
}
