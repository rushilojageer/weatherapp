package com.weather.rushilojageer.weatherapplication.presentation;

import android.support.annotation.NonNull;
import android.util.Log;

import com.weather.rushilojageer.weatherapplication.data.LocationDataSource;
import com.weather.rushilojageer.weatherapplication.data.WeatherDataSource;
import com.weather.rushilojageer.weatherapplication.domain.LocationInfo;
import com.weather.rushilojageer.weatherapplication.domain.WeatherInfo;

import java.lang.ref.WeakReference;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

class WeatherPresenter implements WeatherInfoContract.Presenter {

    private static final String TAG = WeatherPresenter.class.getName();
    private static final double KELVIN_TO_CELSIUS_CONVERSION_CONSTANT = 273.15;
    private static final String DATE_FORMAT = "dd MMMM yyyy";
    private static final String TEMPERATURE_FORMAT_PATTERN = "0";

    private WeakReference<WeatherInfoContract.View> view;
    private WeatherDataSource weatherDataSource;
    private LocationDataSource locationDataSource;

    WeatherPresenter(@NonNull WeatherDataSource weatherDataSource, @NonNull LocationDataSource locationDataSource) {
        this.weatherDataSource = weatherDataSource;
        this.locationDataSource = locationDataSource;
    }

    @Override
    public void attachView(WeatherInfoContract.View view) {
        this.view = new WeakReference<>(view);
    }

    @Override
    public void detachView() {
        view = null;
    }

    @Override
    public void loadWeatherInfo(double latitude, double longitude) {
        weatherDataSource.getWeatherInfo(latitude, longitude, new WeatherDataSource.WeatherInfoCallback() {
            @Override
            public void showProgress() {
                checkViewAttached();
                getView().showLoadingView();
            }

            @Override
            public void hideProgress() {
                checkViewAttached();
                getView().hideLoadingView();
            }

            @Override
            public void onWeatherInfoLoaded(WeatherInfo weatherInfo) {
                checkViewAttached();
                getView().showWeatherInfo(formatDate(new Date()),
                        formatTemperature(convertKelvinToCelsius(weatherInfo.getMinTemperature())),
                        formatTemperature(convertKelvinToCelsius(weatherInfo.getMaxTemperature())));
            }

            @Override
            public void onError() {
                checkViewAttached();
                getView().showWeatherError();
            }
        });
    }

    @Override
    public void loadLocationInfo(double latitude, double longitude) {
        locationDataSource.getLocationInfo(latitude, longitude, new LocationDataSource.LocationInfoCallback() {
            @Override
            public void showProgress() {
                checkViewAttached();
                getView().showLoadingLocation();
            }

            @Override
            public void hideProgress() {
                checkViewAttached();
            }

            @Override
            public void onLocationInfoLoaded(LocationInfo locationInfo) {
                checkViewAttached();

                if(locationInfo.getCountry() == null || locationInfo.getArea() == null) {
                    getView().showLocationInfo(null);
                    return;
                }

                getView().showLocationInfo(String.format("%s, %s", locationInfo.getArea(), locationInfo.getCountry()));
            }

            @Override
            public void onError() {
                checkViewAttached();
                getView().showLocationError();
            }
        });
    }

    public WeatherInfoContract.View getView() {
        return view.get();
    }

    private void checkViewAttached() {
        if (!isViewAttached()) {
            Log.e(TAG, "The view is not attached to the presenter.");
            throw new ViewIsNotAttachedException();
        }
    }

    private boolean isViewAttached() {
        return view != null;
    }

    private String formatDate(Date date) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);
        return dateFormatter.format(date);
    }

    private String formatTemperature(double temperatureInKelvin) {
        DecimalFormat formatter = new DecimalFormat(TEMPERATURE_FORMAT_PATTERN);
        return formatter.format(temperatureInKelvin);
    }

    private double convertKelvinToCelsius(double kelvinTemperature) {
        return kelvinTemperature - KELVIN_TO_CELSIUS_CONVERSION_CONSTANT;
    }

    private static class ViewIsNotAttachedException extends RuntimeException {
        ViewIsNotAttachedException() {
            super("The view is not attached to the presenter.");
        }
    }
}
